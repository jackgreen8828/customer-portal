import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-load',
  templateUrl: './add-load.component.html',
  styleUrls: ['./add-load.component.css']
})
export class AddLoadComponent implements OnInit {

    load: object = {};
    references: object[] = [{}];
    services: string[] = [];
    equipment: string[] = [];

    constructor() { }

    ngOnInit() {
        this.load['stops'] = [];
        this.load['stops'].push({type: 'pickup'});
        this.load['stops'].push({type: 'drop'});
    }

    onSelected(customer: object): void {
        this.load['customer'] = customer;
    }

    addPickup(): void {
        this.load['stops'].push({type: 'pickup'});
    }

    addDrop(): void {
        this.load['stops'].push({type: 'drop'});
    }

    removeStop(index: number): void {
        this.load['stops'].splice(index, 1);
    }

    addReference(): void {
        this.references.push({});
    }

    removeReference(index: number): void {
        this.references.splice(index, 1);
    }

    checkService(service: string): boolean {
        return this.services.includes(service);
    }

    checkEquipment(equipment: string): boolean {
        return this.equipment.includes(equipment);
    }

    toggleService(service: string, checked: boolean): void {
        if (checked) {
            this.services.push(service);
        } else {
            const index = this.services.indexOf(service)
            this.services.splice(index, 1);
        }
    }

    toggleEquipment(equipment: string, checked: boolean): void {
        if (checked) {
            this.equipment.push(equipment);
        } else {
            const index = this.equipment.indexOf(equipment);
            this.equipment.splice(index, 1);
        }
    }

    getExcludes(index: number): string[] {
        const arr = [
            ["addressLine2", "zipCode"],
            ["name", "country"],
            ["state", "email", "comments"]
        ];
        if (index < arr.length) {
            return arr[index];
        }
        return [];
    }
}
