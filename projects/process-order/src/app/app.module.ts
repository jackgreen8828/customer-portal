import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrderComponent } from './components/order/order.component';
import { LoadModule } from './load/load.module';
import { RateModule } from './rate/rate.module';
import { RequestCacheService } from './services/request-cache.service';
import { CachingInterceptorService } from './services/caching-interceptor.service';

@NgModule({
    declarations: [
        AppComponent,
        OrderComponent
    ],
    imports: [
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        BrowserModule,
        LoadModule,
        RateModule
    ],
    providers: [
        RequestCacheService,
        { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptorService, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
