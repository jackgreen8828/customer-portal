import { TestBed } from '@angular/core/testing';

import { ComponentLibService } from './component-lib.service';

describe('ComponentLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComponentLibService = TestBed.get(ComponentLibService);
    expect(service).toBeTruthy();
  });
});
