import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
    selector: '[libElementAttributes]'
})
export class ElementAttributesDirective {

    @Input() attrs: object = {};

    constructor(private e: ElementRef, private renderer: Renderer2) { }

    ngOnInit(): void {
        for (let key in this.attrs) {
            this.renderer.setAttribute(this.e.nativeElement, key, this.attrs[key]);
        }
    }

}
