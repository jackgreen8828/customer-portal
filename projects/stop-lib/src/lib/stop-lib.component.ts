import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'lib-stop-lib',
    templateUrl: './stop-lib.component.html',
    styles: []
})
export class StopLibComponent implements OnInit {

    @Input() stop: object;
    @Input() exclude: string[] = [];
    productColumns: object[] = [];
    hiddenColumns: object[] = [];

    constructor() { }

    ngOnInit() {
        this.stop['products'] = [];
        this.productColumns = [
            { title: 'Product Number', data: 'productNumber', type: 'select' },
            { title: 'Description', data: 'description', type: 'select' },
            { title: 'Quantity', data: 'quantity' },
            { title: 'Total Weight', data: 'totalWeight' },
            { title: 'Total Cube', data: 'totalCube' }
        ];
        this.hiddenColumns = [
            { title: '# of Pieces', data: 'pieces' },
            { title: '# of Parts', data: 'parts' },
            { title: 'Weight', data: 'weight' },
            { title: 'Height', data: 'height' },
            { title: 'Width', data: 'width' },
            { title: 'Length', data: 'length' },
            { title: 'Assembly Minutes', data: 'assemblyMinutes' },
            { title: '# of People', data: 'numberOfPeople' },
            { title: 'NMFC', data: 'nmfc' },
            { title: 'Model Number', data: 'modelNumber' },
            { title: 'Serial Number', data: 'serialNumber' },
            { title: 'Meter', data: 'meter' },
            { title: 'Condition', data: 'condition' },
            { title: 'ITN', data: 'itn' },
            { title: 'ITN In Date', data: 'itnInDate' }
        ];
    }

    getLocations(): void {
        // this.stopService.getLocations()
        //     .subscribe(locations => this.locations = locations);
    }

    addProduct(product: object): void {
        // this.stop.products.push(product);
    }

}
