import { NgModule } from '@angular/core';
import { InputComponent } from './components/input/input.component';
import { ElementAttributesDirective } from './directives/element-attributes.directive';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ElementAttributesDirective,
        InputComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ElementAttributesDirective,
        InputComponent
    ]
})
export class ComponentLibModule { }
