import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'lib-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

    @Input() label: string;
    @Input() type: string = 'text';
    @Input() modelObject: any;
    @Input() options: any;
    @Input() addClasses: string;
    @Input() overrideClasses: string;
    @Input() attrs: object = {};
    @Output() valueChanged = new EventEmitter<any>();
    classes: string;

    constructor() { }

    ngOnInit() {
        if (this.overrideClasses) {
            this.classes = this.overrideClasses;
        } else {
            if (this.type === 'select') {
                this.classes = 'custom-select ';
            } else {
                this.classes = 'form-control ';
            }
            if (this.addClasses) {
                this.classes += this.addClasses;
            }
        }
    }

    inputValue(value: any) {
        this.valueChanged.emit(value);
    }

}
