import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { CarrierInformationComponent } from './carrier-information/carrier-information.component';
import { ChargesComponent } from './charges/charges.component';

@NgModule({
    declarations: [
        CarrierInformationComponent,
        ChargesComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    exports: [
        CarrierInformationComponent,
        ChargesComponent
    ]
})
export class RateModule { }
