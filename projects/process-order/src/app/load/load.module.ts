import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { StopLibModule } from "stop-lib";
import { AddLoadComponent } from './add-load/add-load.component';
import { CustomerSelectComponent } from './customer-select/customer-select.component';

@NgModule({
    declarations: [
        AddLoadComponent,
        CustomerSelectComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        StopLibModule
    ],
    exports: [
        AddLoadComponent
    ]
})
export class LoadModule { }
