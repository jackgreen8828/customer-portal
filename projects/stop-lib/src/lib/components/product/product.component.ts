import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    @Input() columns: object[] = [];
    @Input() details: object[] = [];
    @Input() data: any[] = [];
    expandedObject: object;
    editingObject: object;
    backupObject: object;

    constructor() { }

    ngOnInit() {
    }

    addProduct(): void {
        let newProduct = {};
        this.data.push(newProduct);
        this.expandProduct(newProduct);
        this.editProduct(newProduct);
    }

    setValue(obj: any, key: string, value: any, callback: () => void): void {
        obj[key] = value;
        callback();
    }

    expandProduct(obj: any): void {
        this.expandedObject = obj;
    }

    collapseProduct(): void {
        this.expandedObject = null;
    }

    isExpanded(obj: any): boolean {
        return obj === this.expandedObject;
    }

    editProduct(obj: any): void {
        this.editingObject = obj;
        // TODO: Create backup of product to be restored if save is canceled
    }

    isEditing(obj: any): boolean {
        return this.editingObject === obj;
    }

    saveProduct(obj: any): void {
        this.editingObject = null;
        // TODO: Save action
    }

    cancelEditing(): void {
        this.editingObject = null;
        // TODO: Restore backup object
    }

    deleteProduct(obj: any): void {
        const index = this.data.indexOf(obj);
        if (index != -1) {
            this.data.splice(index, 1);
        }
    }

    temp(obj: any): void {
        console.log(obj);
    }

}
