import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopLibComponent } from './stop-lib.component';

describe('StopLibComponent', () => {
  let component: StopLibComponent;
  let fixture: ComponentFixture<StopLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
