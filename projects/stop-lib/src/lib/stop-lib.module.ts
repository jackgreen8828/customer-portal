import { NgModule } from '@angular/core';
import { StopLibComponent } from './stop-lib.component';
import { CommonModule } from '@angular/common';
import { ComponentLibModule } from "component-lib";
import { ProductComponent } from './components/product/product.component';

@NgModule({
  declarations: [StopLibComponent, ProductComponent],
  imports: [
      CommonModule,
      ComponentLibModule
  ],
  exports: [StopLibComponent]
})
export class StopLibModule { }
