import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CustomerService } from '../../services/customer.service';

@Component({
    selector: 'app-customer-select',
    templateUrl: './customer-select.component.html',
    styleUrls: ['./customer-select.component.css']
})
export class CustomerSelectComponent implements OnInit {

    customers: object[] = [];
    selectedCustomer: object;
    @Output() selected = new EventEmitter<object>();

    constructor(private customerService: CustomerService) { }

    ngOnInit() {
        this.getCustomers();
    }

    getCustomers(): void {
        this.customerService.getCustomerNames()
            .subscribe(data => this.customers = data['customers']);
    }

    selectCustomer(): void {
        this.customerService.getCustomerAddress(this.selectedCustomer['id'])
            .subscribe(data => this.selectedCustomer['location'] = data);
        this.selected.emit(this.selectCustomer);
    }

}
