import { TestBed } from '@angular/core/testing';

import { StopLibService } from './stop-lib.service';

describe('StopLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StopLibService = TestBed.get(StopLibService);
    expect(service).toBeTruthy();
  });
});
