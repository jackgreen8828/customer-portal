import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class CustomerService {

    private baseUrl = 'http://localhost:8080/customers/';

    constructor(private http: HttpClient) { }

    getCustomerNames(): Observable<object> {
        const url = this.baseUrl + '?companyId=TMS';
        return this.http.get<object>(url)
            .pipe(
                catchError(this.handleError('getCustomerNames', []))
            );
    }

    getCustomerAddress(id: number): Observable<object> {
        const url = this.baseUrl + '/contact?id=' + id;
        return this.http.get<object>(url)
            .pipe(
                catchError(this.handleError('getCustomerAddress', {}))
            );
    }

    /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(operation + 'falied!');
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
